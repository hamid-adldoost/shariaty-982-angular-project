import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-comtwo',
  templateUrl: './comtwo.component.html',
  styleUrls: ['./comtwo.component.css']
})
export class ComtwoComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  navigateToComponentThree() {

    this.router.navigateByUrl('/comthree');
  }

}
