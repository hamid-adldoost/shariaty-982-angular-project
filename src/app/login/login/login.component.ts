import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username = '';
  password = '';

  constructor() { }

  ngOnInit() {
  }

  doLogin() {
    if (this.username === 'admin' && this.password === '123') {
      console.log('login successfull');
    } else {
      console.error('username or password is incorrect..');
    }
  }

}
