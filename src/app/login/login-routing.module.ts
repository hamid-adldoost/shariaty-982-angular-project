import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ComtwoComponent} from '../comtwo/comtwo.component';
import {ForgetPasswordComponent} from './forget-password/forget-password.component';
import {LoginComponent} from './login/login.component';


const routes: Routes = [
  { path: 'forget-password', component: ForgetPasswordComponent },
  { path: 'login', component: LoginComponent },
  { path: '', component: LoginComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoginRoutingModule { }
