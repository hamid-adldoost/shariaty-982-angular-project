import {RouterModule, Routes} from '@angular/router';
import {ComtwoComponent} from './comtwo/comtwo.component';
import {BrowserModule} from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ComthreeComponent} from './comthree/comthree.component';
import {RegistrationComponent} from './registration/registration.component';
import {ConfirmComponent} from './confirm/confirm.component';
import {EmployeeComponent} from './employee/employee.component';

const r2: Routes = [
  {path: '', redirectTo: 'register', pathMatch: 'full'},
  { path: 'comtwo', component: ComtwoComponent },
  { path: 'comthree', component: ComthreeComponent },
  { path: 'register', component: RegistrationComponent},
  { path: 'confirm', component: ConfirmComponent},
  { path: 'employee', component: EmployeeComponent},
  { path: 'login', loadChildren: () => import('./login/login.module').then(m => m.LoginModule)},
  { path: 'reactive', loadChildren: () => import('./reactive/reactive.module').then(m => m.ReactiveModule)}
];
@NgModule({
  imports: [RouterModule.forRoot(r2)],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule { }
