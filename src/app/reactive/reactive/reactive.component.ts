import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-reactive',
  templateUrl: './reactive.component.html',
  styleUrls: ['./reactive.component.css']
})
export class ReactiveComponent implements OnInit {

  profileForm = new FormGroup({
    name : new FormControl(''),
    name2 : new FormControl('')
  });

  constructor() { }

  ngOnInit() {
    this.profileForm.get('name').valueChanges.subscribe(x => {
      console.log('name value', x);
    });
    this.profileForm.get('name2').valueChanges.subscribe(x => {
      this.profileForm.get('name').setValue(x);
    });
  }

  // updateName() {
  //   this.name.setValue('Nancy');
  // }

  onSubmit() {
    console.warn(this.profileForm.value);
  }

}
