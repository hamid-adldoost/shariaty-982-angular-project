import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ReactiveComponent} from './reactive/reactive.component';


const routes: Routes = [
  { path: '', component: ReactiveComponent},
  { path: 'reactive', component: ReactiveComponent}
];


// @ts-ignore
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReactiveRoutingModule { }
