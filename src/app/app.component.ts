import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'session 3 project';

  clickCount = 0;

  increaseClickCount() {
    this.clickCount ++;
  }
}
