import { Injectable } from '@angular/core';
import {Person} from './registration/person';

@Injectable({
  providedIn: 'root'
})
export class ManagePersonService {

  constructor() { }

  savePerson(person: Person) {
    let personArray = [];
    try {
       personArray = JSON.parse(localStorage.getItem('person'));
    } catch (e) {
      personArray = [];
    }
    if (!personArray) {
      personArray = [];
    }
    personArray.push(person);
    localStorage.setItem('person', JSON.stringify(personArray));
  }

  fetchPerson(): Person[] {
    return JSON.parse(localStorage.getItem('person'));
  }

}
