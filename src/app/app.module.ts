import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {FormsModule} from '@angular/forms';
import { ComtwoComponent } from './comtwo/comtwo.component';
import {AppRoutingModule} from './app.routing';
import { ComthreeComponent } from './comthree/comthree.component';
import { RegistrationComponent } from './registration/registration.component';
import { ConfirmComponent } from './confirm/confirm.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatSliderModule} from '@angular/material';
import {DpDatePickerModule} from 'ng2-jalali-date-picker';
import { ShamsiDatePipePipe } from './shamsi-date-pipe.pipe';
import {DropdownModule} from 'primeng/dropdown';
import {TableModule} from 'primeng/table';
import { EmployeeComponent } from './employee/employee.component';
import {HttpClientModule} from '@angular/common/http';
import {ButtonModule} from 'primeng/button';

@NgModule({
  declarations: [
    AppComponent,
    ComtwoComponent,
    ComthreeComponent,
    RegistrationComponent,
    ConfirmComponent,
    ShamsiDatePipePipe,
    EmployeeComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    DpDatePickerModule,
    MatSliderModule,
    DropdownModule,
    TableModule,
    HttpClientModule,
    ButtonModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
